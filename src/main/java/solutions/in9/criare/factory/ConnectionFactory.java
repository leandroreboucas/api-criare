
package solutions.in9.criare.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import solutions.in9.criare.util.PropertiesUtil;

/**
 *
 * @author Leandro Rebouças
 */
public class ConnectionFactory {
    
    private final  String ipServer;
    private final  String port;
    private final  String dbName;
    private final  String user;
    private final  String password;

    public ConnectionFactory() throws Exception {
        PropertiesUtil pu = new PropertiesUtil("config.properties");
        this.ipServer = pu.getProp("HOST");
        this.port = pu.getProp("PORT");
        this.dbName = pu.getProp("DBNAME");
        this.user = pu.getProp("USER");
        this.password = pu.getProp("PASSWORD");
    }
    
    public static Connection getPoolConnection() throws NamingException, SQLException {
        InitialContext initCtx = new InitialContext();
        DataSource ds = (DataSource) initCtx.lookup("java:/comp/env/jdbc/nossomunicipio");
        return ds.getConnection();
    }
    
    
    
    public Connection getConnection() throws Exception {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection("jdbc:postgresql://" + this.ipServer + ":" + this.port +
                "/" + this.dbName, this.user, this.password);
    }

    public void getCloseConnection(ResultSet rs, PreparedStatement ps, java.sql.Connection conn) throws SQLException{
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
}
