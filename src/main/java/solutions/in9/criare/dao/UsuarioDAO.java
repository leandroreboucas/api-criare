package solutions.in9.criare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import solutions.in9.criare.factory.ConnectionFactory;
import solutions.in9.criare.model.Usuario;

/**
 * @brief Classe UsuarioDAO
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date 28/04/2017
 */
public class UsuarioDAO {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    private Long _getCodigo(Usuario usuario) throws Exception {
        Long retorno = 0L;
        String sql = ""
                + "SELECT "
                + "  usr_codigo "
                + "FROM "
                + "  fr_usuario "
                + "WHERE "
                + "  usr_login = ?";
        try {
            this.conn = new ConnectionFactory().getConnection();
            this.ps = this.conn.prepareStatement(sql);
            this.ps.setString(1, usuario.getUsr_login());
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                retorno = this.rs.getLong("usr_codigo");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
        return retorno;
    }

    public Usuario login(Usuario usuario) throws Exception {
        Usuario user = new Usuario();
        String sql = ""
                + "SELECT "
                + "  usr_codigo,"
                + "  usr_login,"
                + "  usr_senha "
                + "FROM "
                + "  fr_usuario "
                + "WHERE "
                + "  usr_login = ? AND"
                + "  usr_senha = md5(? || ?);";
        try {
            user.setUsr_codigo(this._getCodigo(usuario));
            if (user.getUsr_codigo() > 0) {
                System.out.println("Entrou aqui");
                this.conn = new ConnectionFactory().getConnection();
                this.ps = this.conn.prepareStatement(sql);
                this.ps.setString(1, usuario.getUsr_login());
                this.ps.setLong(2, user.getUsr_codigo());
                this.ps.setString(3, usuario.getUsr_senha());
                System.out.println(this.ps.toString());
                this.rs = this.ps.executeQuery();
                while(this.rs.next()){
                    user.setUsr_login(this.rs.getString("usr_login"));
                    user.setUsr_senha(this.rs.getString("usr_senha"));
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
        return user;
    }

}
