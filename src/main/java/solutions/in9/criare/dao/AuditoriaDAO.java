package solutions.in9.criare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import solutions.in9.criare.factory.ConnectionFactory;
import solutions.in9.criare.model.Auditoria;

/**
 * @brief Classe AuditoriaDAO
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date 29/04/2017
 */
public class AuditoriaDAO {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public List<Auditoria> getAll(Long codigo) throws Exception {
        List<Auditoria> auditorias = new ArrayList<>();
        try {
            String sql = ""
                    + "SELECT "
                    + "  apu.codigo,"
                    + "  em.razao_social As mantenedora,"
                    + "  se.descricao As secretaria,"
                    + "  ga.descricao As grupo,"
                    + "  pr.descricao As programa,"
                    + "  ta.descricao As tipo_auditoria,"
                    + "  un.descricao As unidade,"
                    + "  to_char(apu.data_inicial,'DD/MM/YYYY') as data_inicial,"
                    + "  to_char(apu.data_final,'DD/MM/YYYY') as data_final "
                    + "FROM"
                    + "  auditoria au "
                    + "  INNER JOIN empresa em ON au.empresa_codigo = em.codigo"
                    + "  INNER JOIN secretaria se ON au.secretaria_codigo = se.codigo"
                    + "  INNER JOIN auditoria_grupo ag ON au.numero_controle = ag.numero_auditoria"
                    + "  INNER JOIN auditoria_programa ap ON ag.numero_controle = ap.numero_auditoria_grupo"
                    + "  INNER JOIN auditoria_programa_unidade apu ON ap.codigo = apu.auditoria_programa_codigo"
                    + "  INNER JOIN grupo_auditoria ga ON ag.grupo_codigo = ga.codigo"
                    + "  INNER JOIN programa pr ON ap.programa_codigo = pr.codigo"
                    + "  INNER JOIN tipo_auditoria ta ON ap.tipo_auditoria_codigo = ta.codigo"
                    + "  INNER JOIN unidade un ON apu.unidade_codigo = un.codigo "
                    + "WHERE "
                    + "  apu.aprovado = true AND"
                    + "  ap.usuario_codigo = ? "
                    + "ORDER BY"
                    + "  apu.codigo";
            this.conn = new ConnectionFactory().getConnection();
            this.ps = this.conn.prepareStatement(sql);
            this.ps.setLong(1, codigo);
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                Auditoria a = new Auditoria();
                a.setCodigo(this.rs.getLong("codigo"));
                a.setMantenedora(this.rs.getString("mantenedora"));
                a.setSecretaria(this.rs.getString("secretaria"));
                a.setGrupo(this.rs.getString("grupo"));
                a.setPrograma(this.rs.getString("programa"));
                a.setTipo_auditoria(this.rs.getString("tipo_auditoria"));
                a.setUnidade(this.rs.getString("unidade"));
                a.setData_inicial(this.rs.getString("data_inicial"));
                a.setData_final(this.rs.getString("data_final"));
                a.setItens(new ItemDAO().getAll(a.getCodigo()));
                auditorias.add(a);
            }
        } catch (Exception ex) {
            Logger.getLogger(AuditoriaDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
        return auditorias;
    }

}
