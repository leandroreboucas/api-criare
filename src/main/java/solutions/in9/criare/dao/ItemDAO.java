package solutions.in9.criare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import solutions.in9.criare.factory.ConnectionFactory;
import solutions.in9.criare.model.Anexo;
import solutions.in9.criare.model.Item;

/**
 * @brief Classe ItemDAO
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date 29/04/2017
 */
public class ItemDAO {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public List<Item> getAll(Long codigo) throws Exception {
        List<Item> itens = new ArrayList<>();
        String sql = ""
                + "SELECT   "
                + "  apu.codigo,"
                + "  ap.programa_codigo,"
                + "  ap.tipo_auditoria_codigo,"
                + "  LPad(Cast((Rank() Over (Order By ir.codigo)) As Text), 3, '0') As numero_item,"
                + "  ir.identificacao_item,"
                + "  ir.item As descricao_detalhada,"
                + "  ir.fonte_evidencia,"
                + "  '0' verificacao,"
                + "  '0' resposta,"
                + "  '' evidencia "
                + "FROM"
                + "  auditoria_programa_unidade apu"
                + "  INNER JOIN auditoria_programa ap ON apu.auditoria_programa_codigo = ap.codigo"
                + "  INNER JOIN item_roteiro ir ON ap.programa_codigo = ir.programa_codigo AND ap.tipo_auditoria_codigo = ir.tipo_auditoria_codigo "
                + "WHERE"
                + "  apu.codigo = ? "
                + "ORDER BY"
                + "  ir.codigo";
        try {
            this.conn = new ConnectionFactory().getConnection();
            this.ps = this.conn.prepareStatement(sql);
            this.ps.setLong(1, codigo);
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                Item i = new Item();
                i.setCodigo(this.rs.getLong("codigo"));
                i.setNumero_item(this.rs.getString("numero_item"));
                i.setIdentificacao_item(this.rs.getString("identificacao_item"));
                i.setDescricao_detalhada(this.rs.getString("descricao_detalhada"));
                i.setFonte_evidencia(this.rs.getString("fonte_evidencia"));
                i.setVerificacao(this.rs.getString("verificacao"));
                i.setEvidencia(this.rs.getString("evidencia"));
                i.setResposta(this.rs.getString("resposta"));
                itens.add(i);
            }
        } catch (Exception ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
        return itens;
    }

    public void updateItens(List<Item> itens) throws Exception {
        for (Item item : itens) {
            try {
                _updateItem(item);
                for (Anexo anexo : item.getAnexos()) {
                    _insertAnexo(item, anexo);
                }
            } catch (Exception ex) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
                throw ex;
            }
        }
    }

    private void _updateItem(Item item) throws Exception {
        String sql = ""
                + "UPDATE "
                + "  auditoria_unidade_constatacoes "
                + "SET"
                + "  evidencia = ?, "
                + "  resposta = ? "
                + "WHERE"
                + "  codigo = ?";
        try {
            this.conn = new ConnectionFactory().getConnection();
            this.ps = this.conn.prepareStatement(sql);
            this.ps.setString(1, item.getEvidencia());
            this.ps.setString(2, item.getResposta());
            this.ps.setLong(3, item.getCodigo());
            this.ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
    }

    private void _insertAnexo(Item item, Anexo anexo) throws Exception {
        String sql = ""
                + "INSERT INTO "
                + "  auditoria_item_anexos(item_codigo, descricao, arquivo) "
                + "VALUES(?,?,?);";
        try {
            this.conn = new ConnectionFactory().getConnection();
            this.ps = this.conn.prepareStatement(sql);
            this.ps.setLong(1, item.getCodigo());
            this.ps.setString(2, anexo.getDescricao());
            this.ps.setString(3, anexo.getArquivo());
            this.ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            new ConnectionFactory().getCloseConnection(rs, ps, conn);
        }
    }

}
