package solutions.in9.criare.util;

import com.google.gson.Gson;
import javax.ws.rs.core.Response;

/**
 * @brief Classe ResponseUtil
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date 06/01/2017
 */
public class ResponseUtil {

    public static Response send(Object object) {
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Content-Type", "application/json")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                //.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
                //.header("Access-Control-Allow-Origin","*")

                .entity(new Gson().toJson(object)).build();
    }
    
    public static Response error(Object object) {
        return Response.serverError().entity(new Gson().toJson(object)).build();
    }

}
