package solutions.in9.criare.util;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Leandro Rebouças
 */
public class PropertiesUtil {

    private Properties props = null;

    public PropertiesUtil(String arquivo) throws Exception {
        this.props = new Properties();
        InputStream file = this.getClass().getClassLoader().getResourceAsStream(arquivo);
        this.props.load(file);
    }

    public String getProp(String atr) {
        return this.props.getProperty(atr);
    }
}
