
package solutions.in9.criare.config;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @brief Classe RestConfig
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
@ApplicationPath("api")
public class RestConfig extends ResourceConfig{
    
    public RestConfig() {
        packages("solutions.in9.criare.service");

    }

}
