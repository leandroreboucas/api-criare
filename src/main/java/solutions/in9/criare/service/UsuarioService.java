
package solutions.in9.criare.service;

import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import solutions.in9.criare.dao.UsuarioDAO;
import solutions.in9.criare.model.Usuario;
import solutions.in9.criare.util.ResponseUtil;

/**
 * @brief Classe UsuarioService
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
@Path("usuario")
public class UsuarioService {
    
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response cadastrar(String json) {
        try {
            Usuario usuario = new Gson().fromJson(json, Usuario.class);
            return ResponseUtil.send(new UsuarioDAO().login(usuario));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseUtil.error(ex.getMessage());
        }
    }

}
