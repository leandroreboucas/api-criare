package solutions.in9.criare.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import solutions.in9.criare.dao.ItemDAO;
import solutions.in9.criare.model.Item;
import solutions.in9.criare.util.ResponseUtil;

/**
 * @brief Classe ItemService
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date 04/05/2017
 */
@Path("item")
public class ItemService {

    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response cadastrar(String json) {
        try {
            List<Item> itens = new Gson().fromJson(json,  new TypeToken<List<Item>>(){}.getType());
            new ItemDAO().updateItens(itens);
            return ResponseUtil.send("Inseriu todos os itens");
        } catch (Exception ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseUtil.error(ex.getMessage());
        }
    }
}
