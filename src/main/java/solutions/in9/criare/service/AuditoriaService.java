
package solutions.in9.criare.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import solutions.in9.criare.dao.AuditoriaDAO;
import solutions.in9.criare.util.ResponseUtil;

/**
 * @brief Classe AuditoriaService
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   29/04/2017
 */
@Path("auditoria")
public class AuditoriaService {
    
    @GET
    @Path("{codigo}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response cadastrar(@PathParam("codigo") Long codigo) {
        try {
            return ResponseUtil.send(new AuditoriaDAO().getAll(codigo));
        } catch (Exception ex) {
            Logger.getLogger(AuditoriaService.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseUtil.error(ex.getMessage());
        }
    }

}
