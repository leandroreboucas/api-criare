
package solutions.in9.criare.model;

import java.util.List;

/**
 * @brief Classe Auditoria
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
public class Auditoria {
    
    private Long codigo;
    private String mantenedora;
    private String secretaria;
    private String grupo;
    private String programa;
    private String tipo_auditoria;
    private String unidade;
    private String data_inicial;
    private String data_final;
    private List<Item> itens;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getMantenedora() {
        return mantenedora;
    }

    public void setMantenedora(String mantenedora) {
        this.mantenedora = mantenedora;
    }

    public String getSecretaria() {
        return secretaria;
    }

    public void setSecretaria(String secretaria) {
        this.secretaria = secretaria;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getTipo_auditoria() {
        return tipo_auditoria;
    }

    public void setTipo_auditoria(String tipo_auditoria) {
        this.tipo_auditoria = tipo_auditoria;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getData_inicial() {
        return data_inicial;
    }

    public void setData_inicial(String data_inicial) {
        this.data_inicial = data_inicial;
    }

    public String getData_final() {
        return data_final;
    }

    public void setData_final(String data_final) {
        this.data_final = data_final;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }
    
    

}
