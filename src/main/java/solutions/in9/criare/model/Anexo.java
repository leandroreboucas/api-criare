
package solutions.in9.criare.model;

/**
 * @brief Classe Anexo
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
public class Anexo {
    
    private Long codigo;
    private Long item_codigo;
    private String descricao;
    private String arquivo;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Long getItem_codigo() {
        return item_codigo;
    }

    public void setItem_codigo(Long item_codigo) {
        this.item_codigo = item_codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }
    
    

}
