
package solutions.in9.criare.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @brief Classe Item
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
public class Item {
    
    private Long codigo;
    private String numero_item;
    private String identificacao_item;
    private String descricao_detalhada;
    private String fonte_evidencia;
    private String verificacao;
    private String respondido;
    private String evidencia;
    private String resposta;
    private List<Anexo> anexos;

    public Item() {
        this.anexos = new ArrayList<>();
    }
    
    

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNumero_item() {
        return numero_item;
    }

    public void setNumero_item(String numero_item) {
        this.numero_item = numero_item;
    }

    public String getIdentificacao_item() {
        return identificacao_item;
    }

    public void setIdentificacao_item(String identificacao_item) {
        this.identificacao_item = identificacao_item;
    }

    public String getDescricao_detalhada() {
        return descricao_detalhada;
    }

    public void setDescricao_detalhada(String descricao_detalhada) {
        this.descricao_detalhada = descricao_detalhada;
    }

    public String getFonte_evidencia() {
        return fonte_evidencia;
    }

    public void setFonte_evidencia(String fonte_evidencia) {
        this.fonte_evidencia = fonte_evidencia;
    }

    public List<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<Anexo> anexos) {
        this.anexos = anexos;
    }

    public String getVerificacao() {
        return verificacao;
    }

    public void setVerificacao(String verificacao) {
        this.verificacao = verificacao;
    }

    public String getRespondido() {
        return respondido;
    }

    public void setRespondido(String respondido) {
        this.respondido = respondido;
    }

    public String getEvidencia() {
        return evidencia;
    }

    public void setEvidencia(String evidencia) {
        this.evidencia = evidencia;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }


}
