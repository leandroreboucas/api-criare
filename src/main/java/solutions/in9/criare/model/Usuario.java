
package solutions.in9.criare.model;

/**
 * @brief Classe Usuario
 * @author Leandro Rebouças <contato@leandroreboucas.com>
 * @date   28/04/2017
 */
public class Usuario {
    
    private Long usr_codigo;
    private String usr_login;
    private String usr_senha;

    public Long getUsr_codigo() {
        return usr_codigo;
    }

    public void setUsr_codigo(Long usr_codigo) {
        this.usr_codigo = usr_codigo;
    }

    public String getUsr_login() {
        return usr_login;
    }

    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    public String getUsr_senha() {
        return usr_senha;
    }

    public void setUsr_senha(String usr_senha) {
        this.usr_senha = usr_senha;
    }
    
    

}
